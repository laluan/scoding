<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Weather application</title>
</head>
<body>
    <div>
        <ul>
            @foreach ($errors as $item)
                <li>{{$item}}</li>
            @endforeach
            @if (session('message'))
               <li>{{session('message')}}</li>
            @endif
        </ul>
    </div>


    <div>
        <p>Temperature : {{$temperature}}</p>
        <p>Wind speed: {{$windSpeed}}</p>
        <p>Wind direction: {{$windDirection}}</p>
    </div>
    <form action="{{route('setting.update','city')}}" method="post">
        @csrf
        @method('PUT')
        <h4>Change city</h4>
        <input type="text" name="setting_value" value="{{$city}}">
        <p>* For testing purposes I have used Kaunas and one of the most windy place Wellington</p>
        @error('setting_value')
            <div>{{$message}}</div>
        @enderror
        <input type="submit" value="Save">
    </form>

    <form action="{{route('subscribtion.create')}}" method="post">
        @csrf
        <h4>Subsribe to the wind forecast</h4>
        <input type="text" name="email" placeholder="Email address">
        @error('email')
            <div>{{$message}}</div>
        @enderror

        <input type="submit" value="Subscribe">
    </form>

    <hr>
    <h4>Existing subscribtions</h4>
    <ol>
        @foreach ($subscribtions as $subscribtion)
            <li>{{$subscribtion->email}}</li>
        @endforeach
    </ol>
    <br>
    <a href="/">Go back to Task No. 1</a>

</body>
</html>
