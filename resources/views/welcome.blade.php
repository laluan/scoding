<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Scoding</title>
</head>
<body>
    {{-- {{$tasks}} --}}
    <div>
        <h4>Tasks</h4>
        <ul>
        @if (count($tasks) > 0)
        <ul>
            @foreach ($tasks as $item)
            <li>{{$item->title}}
                <form action="{{route('api.v1.tasks.destroy',$item->id)}}" method="post">
                    @csrf
                    @method("DELETE")
                    <input type="submit" value="DELETE">
                </form>
                <a href="/edit/{{ $item->id}}">Redaguoti</a>
            </li>
            @endforeach
        </ul>
        @else
        <p>Empty TODOs list &#128533;</p>
        @endif


    </div>

    <div>
        <h4>Add new task</h4>
        <form action="{{ route('api.v1.tasks.create')}}" method="post">
            <input type="text" name="title" placeholder="Enter TODO title">
            <input type="submit" value="Create task">
        </form>
    </div>
    <div>
    @if (isset($selectedTask))
    <h4>Edit task</h4>
    <form action="{{ route('api.v1.tasks.update',$selectedTask->id) }}" method="post">
        @csrf
        @method('PUT')
        <input type="text" name="title" value="{{ $selectedTask->title }}" placeholder="Enter TODO title">
        <input type="submit" value="Create task">
    </form>
    </div>
    @endif
    <br>
    <a href="/weather">Go to Task No. 2</a>


</body>
</html>
