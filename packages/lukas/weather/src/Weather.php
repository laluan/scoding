<?php
namespace Lukas\Weather;

use Exception;

class Weather
{
    private $apiKey;
    private $apiUrl = "http://api.openweathermap.org/data/2.5/";
    private $receivedData;
    private $statusCode;

    public function __construct($apiKey,$cityName)
    {
        $this->apiKey = $apiKey;
        $this->getCurrentWeather($cityName);
    }

    public function getCurrentWeather($cityName)
    {
        $endpoint = $this->apiUrl."weather?q=".$cityName."&appid=".$this->apiKey;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $endpoint,['http_errors' => false]);
        $this->statusCode = $response->getStatusCode();

        if($this->statusCode == 200){
            $this->receivedData =json_decode($response->getBody());
        }else{
            abort($this->statusCode);
        }
    }
    public function getWindSpeed()
    {
        return isset($this->receivedData->wind->speed) ? $this->receivedData->wind->speed : null;
    }
    public function getWindDirection()
    {
        return isset($this->receivedData->wind->deg) ? $this->receivedData->wind->deg : null;

    }
    public function getTemperature()
    {
        return isset($this->receivedData->main->temp) ? ($this->receivedData->main->temp/100) : null;
    }
    public function getStatus()
    {
        return $this->statusCode;
    }
}
