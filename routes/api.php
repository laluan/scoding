<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::name('api.')->group(function(){
    Route::prefix('v1')->group(function(){
        Route::name('v1.')->group(function(){
            Route::get('tasks', 'ApiController@getAllTasks')->name('tasks.index');
            Route::get('tasks/{id}', 'ApiController@getTask')->name('tasks.show');
            Route::post('tasks', 'ApiController@createTask')->name('tasks.create');
            Route::put('tasks/{id}',  'ApiController@updateTask')->name('tasks.update');
            Route::delete('tasks/{task}',  'ApiController@deleteTask')->name('tasks.destroy');
        });
    });
});

