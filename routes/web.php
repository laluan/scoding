<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@indexTask1');
Route::get('edit/{task}', 'TaskController@show');
Route::get('weather', 'PageController@indexTask2')->name('weather.index');

Route::put('setting/{name}', 'SettingController@update')->name('setting.update');
Route::post('subscribtion', 'SubscribtionController@store')->name('subscribtion.create');;
