<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $name
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $name)
    {
        $request->validate(['setting_value'=>'required|string']);

        $setting = Setting::where('name',$name)->get()->first();
        $setting->value = $request->setting_value;
        $setting->save();
        return redirect()->route('weather.index')->with('message','City has been changed');;
    }
}
