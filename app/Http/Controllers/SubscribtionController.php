<?php

namespace App\Http\Controllers;

use App\Subscribtion;
use Illuminate\Http\Request;

class SubscribtionController extends Controller
{
    public function store(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|email|unique:subscribtions'
        ]);

        Subscribtion::create($validated);
        return redirect()->route('weather.index')->with('message','Subscribtion saved');
    }
}
