<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Subscribtion;
use App\Task;
use Illuminate\Http\Request;
use Lukas\Weather\Weather;

class PageController extends Controller
{
    public function indexTask1()
    {
        $tasks = Task::all();
        return view('welcome')->with(compact('tasks'));
    }
    public function indexTask2()
    {
        $city = Setting::where('name','city')->get()->first()->value;
        $weather = new Weather(env('OPEN_WEATHER_API_KEY'),$city);

        $temperature = $weather->getTemperature();
        $windSpeed = $weather->getWindSpeed();
        $windDirection = $weather->getWindDirection();

        $subscribtions  = Subscribtion::all();

        return view('weather')->with(compact('temperature','windSpeed','windDirection','city','subscribtions'));
    }
}
