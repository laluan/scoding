<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Task;
use Illuminate\Http\Request;
use Validator;

class ApiController extends Controller
{
    public function getAllTasks()
    {
        //return Task::all();
        $tasks = Task::get()->toJson(JSON_PRETTY_PRINT);
        return response($tasks, 200);
    }
    public function getTask($id)
    {
        //return Task::findOrFail($id);
        if (Task::where('id', $id)->exists()) {
            $task = Task::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($task, 200);
        } else {
            return response()->json([
              "message" => "Task not found"
            ], 404);
        }
    }
    public function createTask(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                "message" => $validator->errors()
            ], 400);
        }

        Task::create($validator->validated());

        return response()->json([
            "message" => "Task created"
        ], 201);
    }
    public function updateTask($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                "message" => $validator->errors()
            ], 400);
        }

        $task = Task::findOrFail($id);
        $task->title = $request->title;
        $task->save();

        return response()->json([
            "message" => "Task updated"
        ], 200);
    }
    public function deleteTask(Task $task)
    {
        $task->delete();

        return response()->json([
            "message" => "Task deleted"
        ], 202);
    }
}
