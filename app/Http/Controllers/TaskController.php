<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function show(Task $task)
    {
        $tasks = Task::all();
        $selectedTask = $task;
        return view('welcome')->with(compact('tasks','selectedTask'));
    }
}
