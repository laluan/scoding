<?php

namespace App\Console\Commands;

use App\Setting;
use App\Subscribtion;
use Illuminate\Console\Command;
use Lukas\Weather\Weather;
use Mail;

class SendSubscribtionEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscribtion:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends notification for subsriber about changed weather via email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscribtions = Subscribtion::all();
        $city = Setting::where('name','city')->get()->first()->value;
        $weather = new Weather(env('OPEN_WEATHER_API_KEY'),$city);

        $windSpeed = $weather->getWindSpeed();

        if ($windSpeed >= 10) {
            foreach ($subscribtions as $item) {
                if ($item->state == null || $item->state == "down") {
                    $editSubsribtion = Subscribtion::find($item->id);
                    $editSubsribtion->state = "up";
                    $editSubsribtion->save();
                    //send email
                    Mail::raw('Currently wind speed is '.$windSpeed, function ($message) use($item) {
                        $message->to($item->email, 'John Doe');
                        $message->subject('Wind speed is over 10 m/s');
                    });
                }
            }
        }else{
            foreach ($subscribtions as $item) {
                if ($item->state == null || $item->state == "up") {
                    $editSubsribtion = Subscribtion::find($item->id);
                    $editSubsribtion->state = "down";
                    $editSubsribtion->save();
                    //send email
                    Mail::raw('Currently wind speed is '.$windSpeed, function ($message) use ($item){
                        $message->to($item->email, 'John Doe');
                        $message->subject('Wind speed is lower than 10 m/s');
                    });
                }
            }
        }
    }
}
