## Scoding Task 1: TODO List
#### Routes:
 Method   | URI
 ---|---
 GET | /                   
 GET | api/user            
 GET | api/v1/tasks        
 POST | api/v1/tasks        
 GET | api/v1/tasks/{id}   
 PUT | api/v1/tasks/{id}   
 DELETE | api/v1/tasks/{task} 
 GET | edit/{task}         


## Scoding Task 2: Weather application
#### Routes:
Method   | URI
---|---
GET | weather             
PUT | setting/{name}      
POST | subscribtion   


### Instructions
1. In .env file:
    - Fill in MySQL database details.
    - Append file with OPEN_WEATHER_API_KEY=[OPEN_WEATHER_API_KEY]
    - Change MAIL_DRIVER value to log
2. Run php artisan migrate (to migrate tables to your database)
3. Run php artisan serve and visit website on http://127.0.0.1:8000


# Links
1. Task 1: http://127.0.0.1:8000/
2. Task 2: http://127.0.0.1:8000/weather

### Mail logs will be located in /storage/logs/laravel.log
>Currently wind speed is 3.1  
>2020-02-04 12:06:34] local.DEBUG: Message-ID: <007b408a0a89f58c36a85adcdcaf015e@swift.generated>
>Date: Tue, 04 Feb 2020 12:06:34 +0000
>Subject: Wind speed is lower than 10 m/s
>From: 
>To: John Doe <info@info.lt>
>MIME-Version: 1.0
>Content-Type: text/plain; charset=utf-8
>Content-Transfer-Encoding: quoted-printable


>Currently wind speed is 3.1  
>[2020-02-04 12:10:16] local.DEBUG: Message-ID: <f087a1ad2969151f6ccccd95e6d9310d@swift.generated>
>Date: Tue, 04 Feb 2020 12:10:16 +0000
>Subject: Wind speed is over 10 m/s
>From: 
>To: John Doe <info2@info.lt>
>MIME-Version: 1.0
>Content-Type: text/plain; charset=utf-8
>Content-Transfer-Encoding: quoted-printable
